# exercicio-02-webservice-python

# Estudar python
## Práticas mais avançadas 
1. Criar um WebService com python.
- Instalar um web framework 
- Criar os END_POINTS:
   1. /
   2. /sobre
   3. /falecom
- Criar ambiente que forneça um relacionamento com usuario.

## Revisando os conteúdos:

0. Programação em par
1. Práticas de DevOps 
  1. Ambientes virtuais para trabalhar com python
  2. GitOps
1. Usar módulos e funções
2. Padrões de projeto e desenvolvimento
3. Planejamento para execução dos Jobs

## Conteúdos para aprendizado:

0. Programação em par 
1. Práticas de DevOps 
  1. Ambientes virtuais para trabalhar com python
  2. GitOps
2. Uso do PIP para instalação/atualização/remoção de modulos
3. Manipulação de Objetos
4. Compreenção do protocolo HTTP (workflow)
5. Templates (HTML)
6. Suporte a CSS e JS
